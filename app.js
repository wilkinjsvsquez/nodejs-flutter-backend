const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");
const bcrypt = require("bcrypt");
const mongoose = require("mongoose");
const User = require("./models/user");
const app = express();
const port = 3000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "public")));

const mongo_Uri =
	"mongodb+srv://wilkinvsquez:N0s3qu3p0n3r@dbflutter.blrb0yg.mongodb.net/";
mongoose.set("strictQuery", false);
mongoose.connect(mongo_Uri, function (err) {
	if (err) {
		throw err;
	} else {
		console.log(`Successfully connected`);
	}
});

//app.get("/", (req, res) => res.send("Hello World!"));

app.post("/register", (req, res) => {
	const { username, email, password } = req.body;
	const user = new User({ username, email, password });
	user.save(err => {
		if (err) {
			res.status(400).send({
				statusCode: res.statusCode,
				message: "Error al registrar usuario",
				user: { username, email },
			});
		} else {
			res.status(200).send({
				statusCode: res.statusCode,
				message: "Usuario registrado exitosamente",
				user: { username, email },
			});
		}
	});
});

app.post("/authenticate", (req, res) => {
	const { username, password } = req.body;
	User.findOne({ username }, (err, user) => {
		if (err) {
			res.status(401).send({
				statusCode: res.statusCode,
				message: "Error al autenticar usuario",
				user: { username },
			});
		} else if (!user) {
			res.status(404).send({
				statusCode: res.statusCode,
				message: "Usuario no encontrado",
				user: { username },
			});
		} else {
			user.isCorrectPassword(password, (err, result) => {
				if (err) {
					res.status(401).send({
						statusCode: res.statusCode,
						message: "Credenciales incorrectas",
						user: { username },
					});
				} else if (result) {
					res.status(200).send({
						statusCode: res.statusCode,
						message: "Usuario Autenticado",
						user: { username },
					});
				} else {
					res.status(400).send({
						statusCode: res.statusCode,
						message: "Bad Request",
						user: { username },
					});
				}
			});
		}
	});
});

app.listen(port, () => console.log(`Running on port ${port}!`));

module.exports = app;
